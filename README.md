# Provision a `t2.micro` dev server in AWS using Terraform

The server runs on the Ubuntu OS, and has Docker installed.

Local Provisioner for Remote-SSH is also provided.

See freeCodeCamp walk-through: [Learn Terraform and AWS by Building a Dev Environment](https://www.freecodecamp.org/news/learn-terraform-and-aws-by-building-a-dev-environment/).

Note that there are some small changes from the code presented in that video:

- `main.tf` is just the AWS EC2 instances. The networking is broken out to `network.tf` similar to [how we do it in GitLab](https://gitlab.com/gitlab-com/sandbox-cloud/project-templates/gcp-sandbox-environment-template/-/tree/main/terraform).
- I attempted to install openSUSE as well as Ubuntu, but I could not get this to work: network timeouts on SSH. Unsure if this is something in the Terraform/AWS setup (though it's the same?) or if the AMIs I found have some special requirements?
- Slight changes to the naming of files, so that they sort a bit better in the VS Code Explorer.
