#!/bin/bash
# SSH should be enabled, but I can't SSH for some reason?
sudo systemctl enable sshd.service
sudo systemctl start sshd.service

sudo zypper refresh

sudo zypper install --force --no-confirm  ca-certificates curl gpg2

sudo zypper install --no-confirm docker python3-docker-compose
sudo sed --in-place 's:DOCKER_OPTS="":DOCKER_OPTS="--data-root /data/vms/docker/lib --exec-root /data/vms/docker/run":g' /etc/sysconfig/docker
sudo mkdir --parents /data/vms/docker
sudo systemctl enable docker
sudo systemctl start docker
sudo usermod --groups docker --append ec2-user
