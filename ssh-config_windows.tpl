Add-Content -Path ~/.ssh/config -Value @'

Host ${host}
  Hostname ${hostname}
  User ${user}
  IdentityFile ${identityfile}
'@
