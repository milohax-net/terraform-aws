resource "aws_key_pair" "mjl_auth" {
  key_name   = "mjl-dev-key"
  public_key = file("~/key/ssh/aws-mjl-dev-key.pub")
}

# resource "aws_instance" "dev-suse" {
#   instance_type          = "t2.micro"
#   ami                    = data.aws_ami.opensuse_ami.id
#   key_name               = aws_key_pair.mjl_auth.key_name
#   vpc_security_group_ids = [aws_security_group.mjl_sg.id]
#   subnet_id              = aws_subnet.mjl_public_subnet.id
#   user_data              = file("docker_suse.tpl")

#   root_block_device {
#     volume_size = 12
#   }

#   tags = {
#     Name = "dev-suse"
#   }

#     provisioner "local-exec" {
#       command = templatefile("ssh-config_${var.host_os}.tpl", {
#       host         = "dev-suse"
#       hostname     = self.public_ip,
#       user         = "ec2-user",
#       identityfile = "~/key/ssh/aws-mjl-dev-key"
#     })
#     interpreter = var.host_os == "windows" ? ["Powershell", "-Command"] : ["bash", "-c"]
#   }
# }

resource "aws_instance" "dev-node" {
  instance_type          = "t2.micro"
  ami                    = data.aws_ami.ubuntu_ami.id
  key_name               = aws_key_pair.mjl_auth.key_name
  vpc_security_group_ids = [aws_security_group.mjl_sg.id]
  subnet_id              = aws_subnet.mjl_public_subnet.id
  user_data              = file("docker_buntu.tpl")

  # root_block_device {
  #   volume_size = 50
  # }

  tags = {
    Name = "dev-node"
  }

  provisioner "local-exec" {
    command = templatefile("ssh-config_${var.host_os}.tpl", {
      host         = "dev-node"
      hostname     = self.public_ip,
      user         = "ubuntu",
      identityfile = "~/key/ssh/aws-mjl-dev-key"
    })
    interpreter = var.host_os == "windows" ? ["Powershell", "-Command"] : ["bash", "-c"]
  }
}
