# See https://us-west-2.console.aws.amazon.com/ec2/v2/home?region=us-west-2#Images:visibility=public-images;v=3;search=:tumbleweed;sort=desc:imageName
# Note: I'm looking in us-west-2 region's AMI table because this is the availability_zone of the subnet specified in main.tf, where the EC2 will deploy   
# Grab the owner for the top one of those, and use the AMI name for the name values.

# Note the asterisks in the values filter: the date and the hash are wildcards, and the most_recent = true will pick the newest AMI

# So far all the opensuse AMIs either don't work (Tumbleweed), or aren't free, and so require opt-in (Leap)
data "aws_ami" "opensuse_ami" {
  most_recent = true
  owners      = ["679593333241"]

  filter {
    name   = "name"
    values = ["openSUSE-Tumbleweed-v20211203-HVM-x86_64*"]
  }
}

data "aws_ami" "ubuntu_ami" {
  most_recent = true
  owners      = ["262212597706"]

  filter {
    name   = "name"
    values = ["aerospike-ubuntu-20.04*"]
  }
}
